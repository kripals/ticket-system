@extends('layouts.app')

@section('title', 'Tickets')

@section('content')
    <section>
        <div class="section-body">
            <div class="card">
                <div class="card-head">
                    <header class="text-capitalize">all server information</header>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="dt_ticket"
                               class="table order-column hover"
                               data-source="{{ route('ticket.list') }}">
                            <thead>
                            <tr>
                                <th width="1%">#</th>
                                <th width="10%">Title</th>
                                <th width="10%">Status</th>
                                <th width="10%">Priority</th>
                                <th width="10%">Owner</th>
                                <th width="10%">Device</th>
                                <th width="10%">Action</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('css/libs/DataTables/jquery.dataTables.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/libs/DataTables/TableTools.min.css') }}"/>
@endpush

@push('scripts')
<script src="{{ asset('js/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('js/pages/dt_ticket.js') }}"></script>
@endpush