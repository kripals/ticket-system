@extends('layouts.app')

@section('title', 'Ticket')

@section('content')
    <div class="section-body ticket-show" style="padding:20px;" data-ticket-id="{{$ticket->id}}">
        <div class="card card-underline">
            <div class="card-head">
                <header class="text-primary-dark text-uppercase">
                    Ticket ID: {{$ticket->id}}
                </header>
                <div class="tools">
                    <div class="btn-group">
                        {{--<div class="col-sm-6">--}}
                            {{--<a class="btn btn-md btn-accent-light"--}}
                               {{--href="--}}{{--route('booking.provision.create', $order)--}}{{--">TAKE</a>--}}
                        {{--</div>--}}
                        {{--<div class="col-sm-6">--}}
                            <button type="button" class="btn btn-primary-light btn-action dropdown-toggle" data-toggle="dropdown" data-loading-text="<i class='fa fa-spinner fa-pulse'></i> Processing...">
                                Actions
                                <i class="fa fa-caret-down"></i>
                            </button>
                            <ul class="dropdown-menu animation-expand" role="menu">
                                {{-- Status code --}}
                                <li class="dropdown-header" title="{{ $ticket->status }}">
                                    <span class="hidden-xs text-default-dark text-primary text-lg">CURRENT STATUS: </span>{{ $ticket->status }}
                                </li>

                                {{-- Ticket Processes --}}
                                @if($ticket->status == 'NEW')
                                    <li>
                                        <a href="javascript:void(0);"
                                           class="change-status text-capitalize">{{ 'take' }}</a>
                                    </li>
                                @endif
                                <li>
                                    <a href="javascript:void(0);"
                                       class="change-status text-capitalize">{{ 'stall' }}</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);"
                                       class="change-status text-capitalize">{{ 'close' }}</a>
                                </li>
                            </ul>
                        {{--</div>--}}

                    </div>
                </div>
            </div>
            <div class="card-body">
                {{--TICKET DETAILS--}}
                <div class="row">
                    <div class="col-sm-6">
                        <dl class="dl-horizontal dl-icon text-lg">
                            <dt>
                                <span class="fa fa-fw fa-asterisk fa-lg opacity-50"></span>
                            </dt>
                            <dd>
                                <span class="opacity-50">Ticket Title</span>
                                <br>
                                <span class="text-medium">{{$ticket->title}}</span>
                            </dd>
                        </dl>
                    </div>
                    <div class="col-sm-6">
                        <dl class="dl-horizontal dl-icon text-lg">
                            <dt>
                                <span class="fa fa-fw fa-map-marker fa-lg opacity-50"></span>
                            </dt>
                            <dd>
                                <span class="opacity-50">Priority</span>
                                <br>
                                <span class="text-medium">{{$ticket->priority}}</span>
                            </dd>
                        </dl>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <dl class="dl-horizontal dl-icon text-lg">
                            <dt>
                                <span class="fa fa-fw fa-map-marker fa-lg opacity-50"></span>
                            </dt>
                            <dd>
                                <span class="opacity-50">Ticket Detail</span>
                                <br>
                                <span class="text-medium">{{$ticket->details}}</span>
                            </dd>
                        </dl>
                    </div>
                    <div class="col-sm-6">
                        <dl class="dl-horizontal dl-icon text-lg">
                            <dt>
                                <span class="fa fa-fw fa-asterisk fa-lg opacity-50"></span>
                            </dt>
                            <dd>
                                <span class="opacity-50">Status</span>
                                <br>
                                <span class="text-medium">{{$ticket->status}}</span>
                            </dd>
                        </dl>
                    </div>
                </div>
                {{--{{ dd(auth()->id()) }}--}}
                @if($ticket->taken_by)
                    <div class="row">
                        <div class="col-sm-6">
                            <dl class="dl-horizontal dl-icon text-lg">
                                <dt>
                                    <span class="fa fa-fw fa-map-marker fa-lg opacity-50"></span>
                                </dt>
                                <dd>
                                    <span class="opacity-50">Ticket Owner</span>
                                    <br>
                                    <span class="text-medium">{{$ticket->taken_by->name}}</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                @endif
                @if($ticket->stalled_by)
                    <div class="row">
                        <div class="col-sm-6">
                            <dl class="dl-horizontal dl-icon text-lg">
                                <dt>
                                    <span class="fa fa-fw fa-map-marker fa-lg opacity-50"></span>
                                </dt>
                                <dd>
                                    <span class="opacity-50">Stalled Owner</span>
                                    <br>
                                    <span class="text-medium">{{$ticket->stalled_by->name}}</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                @endif
                @if($ticket->closed_by)
                    <div class="row">
                        <div class="col-sm-6">
                            <dl class="dl-horizontal dl-icon text-lg">
                                <dt>
                                    <span class="fa fa-fw fa-map-marker fa-lg opacity-50"></span>
                                </dt>
                                <dd>
                                    <span class="opacity-50">Closed Owner</span>
                                    <br>
                                    <span class="text-medium">{{$ticket->closed_by->name}}</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                @endif

                {{--DEVICE DETAILS--}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="text-default-light text-xl text-primary-dark">Device Details</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <dl class="dl-horizontal dl-icon text-lg">
                                <dt>
                                    <span class="fa fa-fw fa-user fa-lg opacity-50"></span>
                                </dt>
                                <dd>
                                    <span class="opacity-50">Device Name</span>
                                    <br>
                                    <span class="text-medium">{{$ticket->device->service_name}}</span>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section-body" style="padding:20px;">
        <div class="card card-underline">
            <div class="card-head">
                <header class="text-primary-dark text-uppercase">
                    Comments
                </header>
            </div>
            <div class="card-body">
                {{--COMMENTS--}}
                @if($ticket->comments->isNotEmpty())
                    @foreach($ticket->comments as $comment)
                        <div class="row">
                            <div class="col-sm-6">
                                <dl class="dl-horizontal dl-icon text-lg">
                                    <dt>
                                        <span class="fa fa-fw fa-asterisk fa-lg opacity-50"></span>
                                    </dt>
                                    <dd>
                                        <span class="opacity-50">{{ $comment->user->name }}</span>
                                        <br>
                                        <span class="text-medium">{{ $comment->comment }}</span>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="row">
                        <div class="col-sm-6">
                            No Comments
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <section>
        <div class="section-body">
            <div class="card">
                <div class="card-head">
                    <header>Create a Comment</header>
                </div>
                {{ Form::model($ticket, [
                    'route' => ['ticket.comment', $ticket->id],
                    'class' => 'form form-validate',
                    'method' => 'PUT',
                    'role' => 'form',
                ]) }}
                    <div class="card-body floating-label">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {{ Form::textarea('comment', old('comment'),['class' => 'form-control', 'rows' => 2]) }}
                                    {{ Form::label('comment', 'Comment') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-actionbar">
                        <div class="card-actionbar-row">
                            <button type="submit" class="btn btn-flat btn-primary ink-reaction">Comment</button>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </section>
@endsection

@push('scripts')
<script src ="{{asset('js/ticket_change_status.js') }}"></script>
@endpush