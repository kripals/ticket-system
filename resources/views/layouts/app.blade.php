<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="datetime" content="{{ date('Y-m-d H:i:s') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Meta Tags -->
    <meta name="description" content="system">
    <meta name="author" content="Kripal Shrestha">

    <!-- Title-->
    <title>{{ config('app.name') }} - @yield('title', 'Page')</title>

    <!-- Styles -->
    <link href="//fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('css/materialadmin-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/materialadmin.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/material-design-iconic-font.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/libs/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">

    <!-- Page Level Styles -->
    @stack('styles')
</head>
<body class="menubar-hoverable header-fixed menubar-pin"><!--for fixed sidebar menubar-pin-->
@if (auth()->guest())
    @yield('guest')
@else
    <!-- BEGIN HEADER -->
    @include('layouts.partials.header')
    <!-- END HEADER -->
    <!-- BEGIN BASE-->
    <div id="base">
        <!-- BEGIN CONTENT-->
        <div id="content">
            @include('layouts.partials.errors')
            @yield('content')
        </div>
        <!-- END CONTENT -->
        @include('layouts.partials.menubar')
    </div>
    <!-- END BASE -->
@endif

<!-- Global Script For Setting Session Messages and Active URL -->
@include('layouts.partials.global-script')

<!-- Scripts -->
<script src="{{ asset('js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('js/jquery-1.10.7.min.js') }}"></script>
<script src="{{ asset('js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ asset('js/libs/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/libs/spin.js/spin.min.js') }}"></script>
<script src="{{ asset('js/libs/autosize/jquery.autosize.min.js') }}"></script>
<script src="{{ asset('js/libs/bootbox/bootbox.min.js') }}"></script>
<script src="{{ asset('js/libs/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('js/libs/nanoscroller/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ asset('js/core/source/App.min.js') }}"></script>
<script src="{{ asset('js/core/source/AppNavigation.min.js') }}"></script>
<script src="{{ asset('js/core/source/AppCard.min.js') }}"></script>
<script src="{{ asset('js/core/source/AppForm.min.js') }}"></script>
<script src="{{ asset('js/core/source/AppVendor.min.js') }}"></script>
<script src="{{ asset('js/core/source/AppToast.min.js') }}"></script>
<script src="{{ asset('js/core/source/AppBootBox.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/DataTables/datatables.min.js') }}"></script>

<!-- Page Level Scripts -->
@stack('scripts')

</body>
</html>
