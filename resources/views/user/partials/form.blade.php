<div class="card-body floating-label">
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        {{ Form::text('username', old('username'),['class' => 'form-control', 'required', isset($user) ? 'readonly': '']) }}
                        {{ Form::label('username', 'Username') }}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        {{ Form::email('email', old('email'),['class' => 'form-control', 'required', isset($user) ? 'readonly': '']) }}
                        {{ Form::label('email', 'email') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::text('first_name', old('first_name'),['class' => 'form-control']) }}
                {{ Form::label('first_name', 'First Name') }}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::text('last_name', old('last_name'),['class' => 'form-control']) }}
                {{ Form::label('last_name', 'Last Name') }}
            </div>
        </div>
    </div>
    @unless(isset($user))
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    {{ Form::password('password', ['class' => 'form-control', 'id' => 'password', 'required']) }}
                    {{ Form::label('password', 'Password') }}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    {{ Form::password('password_confirmation', ['class' => 'form-control', 'required']) }}
                    {{ Form::label('password_confirmation', 'Confirm Password') }}
                </div>
            </div>
        </div>
    @endunless
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::text('address', old('address'),['class' => 'form-control']) }}
                {{ Form::label('address', 'Address') }}
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::text('contact_no', old('contact_no'),['class' => 'form-control']) }}
                {{ Form::label('contact_no', 'Contact No') }}
            </div>
        </div>
    </div>
</div>
<div class="card-actionbar">
    <div class="card-actionbar-row">
        <button type="reset" class="btn btn-flat ink-reaction">Reset</button>
        <button type="submit" class="btn btn-flat btn-primary ink-reaction">Save</button>
    </div>
</div>
