@extends('emails.layout')

@section('heading', 'Change Status')

@section('content')
    A Ticket was created Ticket No:{{ $ticket->id }}
    <b>({{ $ticket->title }})</b>
    Detail {{ $ticket->details }}
    has the status of {{ $ticket->status }}.
@endsection

