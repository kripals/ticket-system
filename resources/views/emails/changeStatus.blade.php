@extends('emails.layout')

@section('heading', 'Change Status')

@section('content')
    The Status of Ticket No:{{ $ticket->id }}
    <b>({{ $ticket->title }})</b>
    has been changed to {{ $ticket->status }}.
@endsection

