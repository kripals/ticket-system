<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::post('user/list', 'UserController@userList')->name('user.list');
Route::post('ticket/list', 'TicketController@ticketList')->name('ticket.list');

Route::post('ticket/change-status', 'TicketController@changeStatus')->name('ticket.change.status');
