<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();

/*
|--------------------------------------------------------------------------
| Logging In/Out Routes
|--------------------------------------------------------------------------
*/
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');

Route::group([ 'middleware' => 'auth' ], function () {
    Route::get('/', 'HomeController@index')->name('home');

    /*
     |--------------------------------------------------------------------------
     | Admin User CRUD Routes
     |--------------------------------------------------------------------------
     */
    Route::group([ 'as' => 'user.', 'prefix' => 'user' ], function () {
        Route::get('', 'UserController@index')->name('index');
        Route::get('create', 'UserController@create')->name('create');
        Route::post('', 'UserController@store')->name('store');
        Route::get('{user}/edit', 'UserController@edit')->name('edit');
        Route::put('{user}', 'UserController@update')->name('update');
        Route::delete('{user}', 'UserController@destroy')->name('destroy');
    });

    /*
     |--------------------------------------------------------------------------
     | Ticket Routes
     |--------------------------------------------------------------------------
     */
    Route::group([ 'as' => 'ticket.', 'prefix' => 'ticket' ], function () {
        Route::get('', 'TicketController@index')->name('index');
        Route::get('{ticket}/details', 'TicketController@show')->name('show');
//        Route::get('create', 'TicketController@create')->name('create');
//        Route::post('', 'TicketController@store')->name('store');
//        Route::get('{user}/edit', 'TicketController@edit')->name('edit');
        Route::put('comment/{ticket}', 'TicketController@comment')->name('comment');
//        Route::delete('{user}', 'TicketController@destroy')->name('destroy');
    });
});