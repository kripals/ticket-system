<?php

return [
    /* Layout ------------------------------ */
    'body'                => 'margin: 0; padding: 0; width: 100%; height: 100%; background-color: #f6f6f6; box-sizing: border-box;',
    'email-wrapper'       => 'margin: 0 auto; padding: 20px;',
    'email-content'       => 'width: 70%; border: 1px solid #e6e6e6; border-radius: 3px; background-color: #fff;',

    /* Masthead ----------------------- */
    'email-masthead'      => 'padding: 25px 0; text-align: center;',
    'email-masthead_name' => 'font-size: 16px; font-weight: bold; color: #000; text-decoration: none; text-shadow: 0 1px 0 white;',

    /* Body ------------------------------ */
    'email-body'          => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;',
    'email-body_inner'    => 'width: 100%; margin: 0; padding: 0;',
    'email-body_cell'     => 'padding: 35px;',

    'email-body_table' => 'padding: 0 100px 10px; text-align: center; width: 100%;',

    'email-footer'      => 'width: 100%; margin: 0; padding: 0; text-align: center;',
    'email-footer_cell' => 'color: #000; padding: 35px; text-align: center;',

    /* Type ------------------------------ */
    'anchor'            => 'color: #3869D4;',
    'header-1'          => 'margin-top: 5px; color: #000; font-size: 19px; font-weight: bold; text-align: center;',
    'header-2'          => 'margin: 1em 0 0 0; color: #000; font-size: 16px; font-weight: bold; text-align: center;',
    'paragraph'         => 'margin-top: 0; color: #000; font-size: 16px; line-height: 1.5em; text-align: justify;',
    'paragraph-sub'     => 'margin-top: 0; color: #000; font-size: 12px; line-height: 1.5em;',
    'paragraph-center'  => 'text-align: center;',
    'code'              => 'padding: 20px; background-color: #ddd; border-radius: 5px;',
    'table-head'        => 'width: 50%; border-top: 0; border-bottom: 2px solid #e6e6e6; vertical-align: bottom; padding: 10px;',
    'table-data'        => 'width: 50%; border-top: 0; border-bottom: 1px solid #e6e6e6; vertical-align: bottom; padding: 10px;',
    'social-anchor'     => 'text-decoration: none;',
    'social-img'        => 'margin-right: 5px;',

    /* Buttons ------------------------------ */
    'button'            => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                     background-color: #3869D4; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                     text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',

    'button--green' => 'background-color: #22BC66;',
    'button--red'   => 'background-color: #dc4d2f;',
    'button--blue'  => 'background-color: #3869D4;',

    /* Font Family--------------------------- */
    'font-family'   => 'font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif;'
];