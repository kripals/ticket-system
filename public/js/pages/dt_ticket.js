(function (namespace, $) {
    "use strict";

    var TicketDataTable = function () {
        var o = this;
        $(document).ready(function () {
            o.initialize();
        });
    };
    var p = TicketDataTable.prototype;

    p.initialize = function () {
        this._initDataTables();
    };

    p._initDataTables = function () {
        if (!$.isFunction($.fn.dataTable)) {
            return;
        }

        this.createDataTable();
    };

    p.createDataTable = function () {
        var $dt_ticket = $("#dt_ticket");

        var table = $dt_ticket.DataTable({
            "dom": "rBftip",
            "language": {
                "processing": "<h2 id='dt_loading'><span class='fa fa-spinner fa-pulse'></span> Loading...</h2>"
            },
            "buttons": [
                'pageLength', 'colvis',
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: ':visible'
                    },
                    orientation: 'landscape',
                    pageSize: 'LEGAL'
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                }
            ],
            "processing": true,
            "serverSide": true,
            "ajax": {
                "type": "POST",
                "url": $dt_ticket.data("source")
            },
            "lengthMenu": [[50, 100, -1], [50, 100, "All"]],
            "pageLength": "50",
            "order": [[0, "asc"]],
            "columns": [
                {
                    "data": "id",
                    "name": "id"
                },
                {
                    "data": "title",
                    "name": "title"
                },
                {
                    "data": "status",
                    "name": "status"
                },
                {
                    "data": "priority",
                    "name": "priority"
                },
                {
                    "data": "owner",
                    "name": "owner"
                },
                {
                    "data": "device",
                    "name": "device"
                },
                {
                    "data": "action",
                    "class": "text-right",
                    "orderable": false,
                    "searchable": false,
                    "render": function (data) {
                        return data ? data : '-';
                    }
                }
            ]
            // "createdRow": function (row, data) {
            //     if (data['service_state'] === "0" || data['service_state'] === "1") {
            //         $(row).addClass('danger');
            //     } else if (data['service_state'] === "2") {
            //         $(row).addClass('warning');
            //     } else {
            //         $(row).addClass('success');
            //     }
            // }
        });
    };
    window.materialadmin.TicketDataTable = new TicketDataTable;
}(this.materialadmin, jQuery));