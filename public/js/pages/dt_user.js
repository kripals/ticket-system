(function (namespace, $) {
   "use strict";

   var UserDataTable = function () {
       var o = this;
       $(document).ready(function () {
           o.initialize();
       });
   };

   var p = UserDataTable.prototype;

   p.initialize = function () {
       this._initDataTables();
   };

   p._initDataTables = function () {
     if(!$.isFunction($.fn.dataTable))
     {
         return;
     }
     this.createDataTable();
   };

   p.createDataTable = function () {
       var $dt_user = $('#dt_user');
       var table = $dt_user.DataTable({
          "dom"     : "rBftip",
          "language": {
              "processing": "<h2 id='dt_loading'><span class='fa fa-spinner fa-pulse'></span> Loading...</h2>"
          },
           "buttons": [
               'pageLength', 'excel', 'pdf', 'print', 'colvis'
           ],
           "processing" : true,
           "serverSide" : true,
           "ajax":{
              "type"    : "POST",
               "url"    : $dt_user.data('source')
           },
           "pageLength" : "50",
           "order"      : [],
           "columns"    : [
               {
                   "class"          : 'details-control text-center',
                   "data"           : null,
                   "defaultContent" : '',
                   "searchable"     : false,
                   "orderable"      : false
               },
               {"data": "id"},
               {"data": "name"},
               {"data": "email"},
               {"data": "contact_no"},
               {
                   "data": "action",
                   "name": "action",
                   "class": "text-right",
                   "orderable": false,
                   "searchable":false
               }
           ],
           "drawCallback": function () {
               $('[data-toggle="tooltip"]').tooltip();
           }
       });
   };

   window.materialadmin.UserDataTable = new UserDataTable;
} (this.materialadmin, jQuery));