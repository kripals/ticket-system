$(document).ready(function() {
    function changeStatus(status) {
        var $btn = $(".btn-action");
        $.ajax({
            "type": "POST",
            "url": "/api/ticket/change-status",
            "data": {
                "id": $(".ticket-show").data("ticket-id"),
                "status": status
            },
            "beforeSend": function () {
                $btn.button('loading');
            },
            "success": function (response) {
                $("#ticket-status").text(response.status);
                setTimeout(function(){window.location.reload();});
            },
            "error": function (response) {
                console.log(response);
                bootbox.alert("<p>" + response.responseText + "</p>");
            },
            "complete": function () {
                $btn.button('reset');
            }
        })
    }

   $(document).on("click", ".change-status", function () {
       var status = $(this).text();

       bootbox.confirm({
          message: "<p> Are you sure you want to change the status of this ticket? </p>",
           buttons: {
              confirm:{
                  label: 'Yes',
                  className: 'btn-raised btn-success'
              },
               cancel:{
                  label: 'No',
                   className: 'btn-raised btn-danger'
               }
           },
           callback: function (response) {
               if(response)
               {
                   changeStatus(status);
               }
           }
       });
   });
});