<?php

namespace App\Models;

use App\Device;
use Illuminate\Database\Eloquent\Model;
use Venturecraft\Revisionable\RevisionableTrait;

class Ticket extends Model
{
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'details',
        'status',
        'priority',
        'device_id'
    ];

    /**
     * The attributes appended in the JSON form.
     *
     * @var array
     */
    protected $appends = [
        'taken_by',
        'stalled_by',
        'closed_by'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return null|User
     */
    public function getTakenByAttribute()
    {
        $createdHistory = $this->revisionHistory->where('key', 'status')->where('new_value', 'OPEN')->first();
        if ($createdHistory)
        {
            return User::find($createdHistory->user_id);
        }
        return null;
    }

    /**
     * @return null|User
     */
    public function getStalledByAttribute()
    {
        $createdHistory = $this->revisionHistory->where('key', 'status')->where('new_value', 'STALLED')->first();
        if ($createdHistory)
        {
            return User::find($createdHistory->user_id);
        }
        return null;
    }

    /**
     * @return null|User
     */
    public function getClosedByAttribute()
    {
        $createdHistory = $this->revisionHistory->where('key', 'status')->where('new_value', 'CLOSED')->first();
        if ($createdHistory)
        {
            return User::find($createdHistory->user_id);
        }
        return null;
    }
}
