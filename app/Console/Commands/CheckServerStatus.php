<?php

namespace App\Console\Commands;

use App\Device;
use App\Mail\TicketCreateNotify;
use App\Models\Ticket;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class CheckServerStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'server:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To check the server status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $process = new Process('/home/nts/getdata.py');
        $process->run();

        // executes after the command finishes
        if ( ! $process->isSuccessful())
        {
            throw new ProcessFailedException($process);
        }

        $data = $process->getOutput();

        //        $data    = '{"Client-Server":{"Uptime":{"state":"0","status":"OK - Up since Sun May  6 11:25:22 2018 (0d 00:50:38)"},"TCP Connections":{"state":"0","status":"OK - ESTABLISHED: 1; TIME_WAIT: 1; LISTEN: 10"},"Postfix status":{"state":"0","status":"OK - Status: the Postfix mail system is running; PID: 1445"},"Postfix Queue":{"state":"0","status":"OK - Deferred queue length: 0; Active queue length: 0"},"OMD NTSmonitoring status":{"state":"0","status":"OK - running"},"OMD NTSmonitoring performance":{"state":"0","status":"OK - Host Checks: 0.0/s; Service Checks: 0.0/s; Process Creations: 0.0/s; Livestatus Connects: 0.0/s; Livestatus Requests: 0.0/s; Log Messages: 0.0/s; 1 Hosts; 10 Services; Core version: 3.5.0; Livestatus version: 1.4.0p31"},"OMD NTSmonitoring apache":{"state":"0","status":"OK - 0.01 Requests/s; 0.00 Seconds serving/s; 0.04 B Sent/s"},"OMD NTSmonitoring Event Console":{"state":"0","status":"OK - Current events: 0; Virtual memory: 216.70 MB; Overall event limit inactive; No hosts event limit active; No rules event limit active; Received messages: 0.00/s; Rule hits: 0.00/s; Rule tries: 0.00/s; Message drops: 0.00/s; Created events: 0.00/s; Client connects: 0.01/s; Rule hit ratio: -; Processing time per message: -; Time per client request: 180.97 ms"},"Number of threads":{"state":"0","status":"OK - 479 threads"},"Mount options of /":{"state":"0","status":"OK - Mount options exactly as expected"},"Memory":{"state":"1","status":"WARN - RAM used: 422.53 MB of 975.20 MB; Swap used: 141.88 MB of 1022.00 MB; Total virtual memory used: 564.41 MB of 1.95 GB (28.3%); Committed: 2.76 GB (141.4% of RAM + Swap; warn/crit at 100.0%/150.0%)(!);"},"Kernel Process Creations":{"state":"0","status":"OK - 2/s"},"Kernel Major Page Faults":{"state":"0","status":"OK - 0/s"},"Kernel Context Switches":{"state":"0","status":"OK - 163/s"},"Interface 2":{"state":"0","status":"OK - [eth0] (up) MAC: 00:0C:29:7B:FF:A9; 1 Gbit/s; in: 22.81 B/s(0.0%); out: 514.32 B/s(0.0%)"},"Filesystem /":{"state":"0","status":"OK - 28.3% used (5.26 of 18.58 GB); trend: +87.85 kB / 24 hours"},"Disk IO SUMMARY":{"state":"0","status":"OK - Utilization: 0.3%; Read: 0.00 B/s; Write: 6.92 kB/s; Average Wait: 6.05 ms; Average Read Wait: 0.00 ms; Average Write Wait: 6.05 ms; Latency: 4.65 ms; Average Queue Length: 0.00"},"Check_MK Discovery":{"state":"1","status":"WARN - Discovery failed: Cannot get data from TCP port 192.168.0.2:6556: [Errno 111] Connection refused"},"Check_MK":{"state":"2","status":"CRIT - Cannot get data from TCP port 192.168.0.2:6556: timed out; execution time 5.0 sec"},"CPU utilization":{"state":"0","status":"OK - user: 1.7%; system: 1.0%; wait: 0.2%; steal: 0.0%; guest: 0.0%; total: 2.9%"},"CPU load":{"state":"0","status":"OK - 15 min load 0.16"}},"ASA1":{"VPN Tunnel 10.10.10.1":{"state":"0","status":"OK - Phase 1: in: 9.21 B/s; out: 9.21 B/s; Phase 2: in: 0.00 B/s; out: 11.01 B/s"},"Uptime":{"state":"0","status":"OK - Up since Wed May  9 08:56:13 2018 (0d 01:38:52)"},"SVC Sessions":{"state":"0","status":"OK - Currently 0 Sessions"},"SNMP Info":{"state":"0","status":"OK - Cisco Adaptive Security Appliance Version 9.6(4); NTS-Monitoring-ASA"},"Mem used System memory":{"state":"0","status":"OK - 61.3% (1.23 GB) of 2.00 GB used"},"Mem used MEMPOOL_GLOBAL_SHARED":{"state":"2","status":"CRIT - 97.0% (30.67 MB) of 31.63 MB used (critical at 90%)"},"Mem used MEMPOOL_DMA":{"state":"0","status":"OK - 24.3% (83.28 MB) of 342.50 MB used"},"Interface 4":{"state":"0","status":"OK - [LAN] (up) MAC: 52:97:05:39:B1:02; 1 Gbit/s; in: 74.93 B/s(0.0%); out: 185.90 B/s(0.0%)"},"Interface 3":{"state":"0","status":"OK - [VPN] (up) MAC: 52:97:05:39:B1:01; 1 Gbit/s; in: 50.45 B/s(0.0%); out: 70.48 B/s(0.0%)"},"Check_MK Discovery":{"state":"0","status":"OK - no unmonitored services found; no vanished services found"},"Check_MK":{"state":"0","status":"OK - execution time 0.7 sec"},"CPU utilization":{"state":"0","status":"OK - 2.0% utilization in the last 5 minutes"}}}';
        $devices = json_decode($data, true);

        foreach ($devices as $keyDevice => $device)
        {
            foreach ($device as $keyState => $state)
            {
                if ($state['state'] == 2)
                {
                    $service = [
                        'service_name'   => $keyDevice . '[' . $keyState . ']',
                        'service_state'  => $state['state'],
                        'service_status' => $state['status']
                    ];

                    $ser = Device::create($service);

                    $ticket = [
                        'title'     => $keyDevice . '[' . $keyState . ']',
                        'slug'      => str_slug($keyDevice . '-' . $keyState),
                        'details'   => $state['status'],
                        'status'    => 'NEW',
                        'priority'  => 'CRITICAL',
                        'device_id' => $ser->id,
                    ];

                    $tick = Ticket::create($ticket);

                    Mail::to(ticket_notifiable())->send(new TicketCreateNotify($tick));
                }
                elseif ($state['state'] == 1)
                {
                    $service = [
                        'service_name'   => $keyDevice . '[' . $keyState . ']',
                        'service_state'  => $state['state'],
                        'service_status' => $state['status']
                    ];

                    $ser = Device::create($service);

                    $ticket = [
                        'title'     => $keyDevice . '[' . $keyState . ']',
                        'slug'      => str_slug($keyDevice . '-' . $keyState),
                        'details'   => $state['status'],
                        'status'    => 'NEW',
                        'priority'  => 'NORMAL',
                        'device_id' => $ser->id,
                    ];

                    $tick = Ticket::create($ticket);

                    Mail::to(ticket_notifiable())->send(new TicketCreateNotify($tick));
                }
            }
        }
    }
}
