<?php
use App\Models\SmsRecord;
use Illuminate\Support\Facades\DB;

/**
 * @param $value
 * @param string $dash
 * @return string
 */
function display($value, $dash = 'NA')
{
    if (empty($value))
    {
        return $dash;
    }

    return $value;
}

/**
 * @param $width
 * @param null $username
 * @return mixed
 * @internal param $guard
 */
function user_avatar($width, $username = null)
{
    if ($username)
    {
        $user = \App\Models\User::whereUsername($username)->first();
    }
    else
    {
        $user = auth()->user();
    }

    if ($image = $user->image)
    {
        return asset($image->thumbnail($width, $width));
    }
    else
    {
        return asset(config('paths.placeholder.avatar'));
    }
}

/**
 * @param $width
 * @param null $entity
 * @return mixed
 */
function thumbnail($width, $entity = null)
{
    if ( ! is_null($entity))
    {
        if ($image = $entity->image)
        {
            return asset($image->thumbnail($width, $width));
        }
    }

    return asset(config('paths.placeholder.default'));
}

/**
 * @param $query
 * @return mixed
 */
//function setting($query)
//{
//    $setting = \App\Models\Setting::fetch($query)->first();
//
//    return $setting ? $setting->value : null;
//}

/**
 * @return \Illuminate\Support\Collection
 */
function ticket_notifiable()
{
    $user = App\Models\User::where('id', '1')->pluck('email');

    return $user;
}