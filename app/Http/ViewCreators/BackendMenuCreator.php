<?php

namespace App\Http\ViewCreators;

use Illuminate\View\View;

class BackendMenuCreator
{

    /**
     * The user model.
     *
     * @var \App\Models\User;
     */
    protected $user;

    /**
     * Create a new menu bar composer.
     */

    /**
     * Bind data to the view.
     *
     * @internal param View $view
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }

    public function create(View $view)
    {
        $menu[] = [
            'class' => false,
            'route' => url('/'),
            'icon'  => 'md md-home',
            'title' => 'Home'
        ];

        array_push($menu, [
            'class' => false,
            'route' => route('user.index'),
            'icon'  => 'md md-web',
            'title' => 'User'
        ]);

        array_push($menu, [
            'class' => false,
            'route' => route('ticket.index'),
            'icon'  => 'md md-list',
            'title' => 'Ticket'
        ]);

        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('service.index'),
        //            'icon'  => 'md md-list',
        //            'title' => 'Service'
        //        ]);
        //
        //        //        array_push($menu, [
        //        //            'class' => false,
        //        //            'route' => route('driver-info.index'),
        //        //            'icon'  => 'md md-web',
        //        //            'title' => 'Driver Info'
        //        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('gallery.index'),
        //            'icon'  => 'md md-list',
        //            'title' => 'Gallery'
        //        ]);
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('news.index'),
        //            'icon'  => 'md md-list',
        //            'title' => 'News'
        //        ]);
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('notice.index'),
        //            'icon'  => 'md md-list',
        //            'title' => 'Notice'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('vendor-info.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Vendor Info'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('car-type.index'),
        //            'icon'  => 'md md-list',
        //            'title' => 'Vehicle Type'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('car-info.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Vehicle Info'
        //        ]);
        //
        //        //        array_push($menu, [
        //        //            'class' => false,
        //        //            'route' => route('package.index'),
        //        //            'icon'  => 'md md-web',
        //        //            'title' => 'Package'
        //        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('price.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Price'
        //        ]);
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('user.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'User'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('booking.provision.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Booking Provision'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('booking.order.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Booking Order'
        //        ]);
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('cml.index'),
        //            'icon'  => 'md md-list',
        //            'title' => 'Vehicle Maintenance Log'
        //        ]);
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('coi.index'),
        //            'icon'  => 'md md-list',
        //            'title' => 'Vehicle Odometer Info'
        //        ]);
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('cti.index'),
        //            'icon'  => 'md md-list',
        //            'title' => 'Vehicle Tax Insurance Info'
        //        ]);
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('enquiry.index'),
        //            'icon'  => 'md md-list',
        //            'title' => 'Enquiries'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('promocode.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Promocode'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('card.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Spark Card'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('customer.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Customer'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('role.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Role'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('report'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Order Report'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => false,
        //            'route' => route('driver-info.index'),
        //            'icon'  => 'md md-web',
        //            'title' => 'Driver Info'
        //        ]);
        //
        //        array_push($menu, [
        //            'class' => 'gui-folder',
        //            'route' => 'javascript:void(0);',
        //            'icon'  => 'fa fa-files-o',
        //            'title' => 'Reports',
        //            'items' => [
        //                [
        //                    'route' => route('vehicle.report.vehicleMaintenanceReport'),
        //                    'title' => 'Vehicle Maintenance Report'
        //                ],
        //                [ 'route' => route('vehicle.report.vehicleHistory'),
        //                  'title' => 'Vehicle Provision History Report'
        //                ],
        //                [
        //                    'route' => route('report.customerOrder'),
        //                    'title' => 'Customer Order Report'
        //                ],
        //                [
        //                    'route' => route('report.driverStatus'),
        //                    'title' => 'Driver Status Report'
        //                ],
        //                [
        //                    'route' => route('report.driverWorkHistory'),
        //                    'title' => 'Driver Work History Report'
        //                ],
        //                [
        //                    'route' => route('report.VehicleOdometerReadingHistory'),
        //                    'title' => 'Vehicle Odometer Reading History'
        //                ],
        //                [
        //                    'route' => route('report.paymentHistory'),
        //                    'title' => 'Payment History'
        //                ],
        //                [
        //                    'route' => route('report.vendorVehicleOrderHistory'),
        //                    'title' => 'Vendor Vehicle Order History'
        //                ]
        //
        //            ]
        //        ]);

        /*
        * Sample for adding menu
        * array_push($menu,
        [
        'class' => {desired class},
        'route' => {desired route or url},
        'icon'  => {md or fa icon class},
        'title' => {title},
        \\Optional Sub Menu Items
        'items' => [
        ['route' => {route or url}, 'title' => {title}],
        ...
        ]
        ]);
        */

        $view->with('allMenu', $menu);
    }
}