<?php

namespace App\Http\Controllers;

use App\Device;
use App\Mail\TicketCreateNotify;
use App\Mail\TicketStatusNotify;
use App\Models\Comment;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;

class TicketController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('ticket.index');
    }

    /**
     * @return mixed
     */
    public function ticketList()
    {
        return DataTables::of(Ticket::get())->addColumn('owner', function ($item)
        {
            if (empty($item->owner))
            {
                return "NA";
            }
            else
            {
                return $item->owner->name;
            }
        })->addColumn('device', function ($item)
        {
            return $item->device->service_name;
        })->addColumn('action', function ($item)
        {
            $buttons = '<a href="' . route('ticket.show', $item->id) . '" class="btn btn-primary btn-flat btn-xs" target="_blank">View</a>';

            return $buttons;
        })->make(true);
    }

    /**
     * @param Ticket $ticket
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Ticket $ticket)
    {
        return view('ticket.show', compact('ticket'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function changeStatus(Request $request)
    {
        $ticket = Ticket::findOrFail($request->id);

        $allStatus = [
            'take'  => 'OPEN',
            'stall' => 'STALLED',
            'close' => 'CLOSE'
        ];

        $message = DB::transaction(function () use ($request, $ticket, $allStatus)
        {
            $message = 'Nothing Happened';

            foreach ($allStatus as $requestStatus => $status)
            {
                if ($request->status == $requestStatus)
                {
                    $ticket->update([
                        'status' => $status,
                    ]);

                    $message = 'Ticket' . ucwords($status) . '!';

                    Mail::to(ticket_notifiable())->send(new TicketStatusNotify($ticket));

                    break;
                }
            }

            return $message;
        });

        return response([
            'message' => $message,
            'status'  => $allStatus[ $request->status ]
        ]);
    }

    /**
     * @param Ticket $ticket
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function comment(Ticket $ticket, Request $request)
    {
        DB::transaction(function () use ($request, $ticket)
        {
            $comment = Comment::create([
                'ticket_id' => $ticket->id,
                'user_id'   => auth()->id(),
                'comment'   => $request->comment
            ]);
        });

        return redirect()->route('ticket.show', $ticket->id);
    }
}
