<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdatePassword;
use App\Http\Requests\UpdateUser;
use App\Models\User;
use App\Role;
use DB;
use Form;
use Illuminate\Support\Facades\Auth;
use Venturecraft\Revisionable\RevisionableTrait;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    use RevisionableTrait;

    protected $revisionCreationsEnabled = true;

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('user.index');
    }

    /**
     * @return mixed
     */
    public function userList()
    {
        return Datatables::of(User::where('username', '<>', 'super_admin')->get())
        ->editColumn('created_at', function ($user)
        {
            return $user->created_at;
        })
        ->addColumn('action', function ($user)
        {
            $buttons = '<a href="' . route('user.edit', $user->username) . '" class="btn btn-primary btn-flat btn-xs">Edit</a>';
            $buttons .= '<button type="button" class="btn btn-primary btn-flat btn-xs item-delete" data-url="' . route('user.destroy', $user->username) . '">Delete</button>';

            return $buttons;
        })->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * @param StoreUser $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreUser $request)
    {
        DB::transaction(function () use ($request)
        {
            $data = $request->data();

            $user = User::create($data);
        });

        return redirect()->route('user.index')->withSuccess('Successfully stored');
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(User $user)
    {
        return view('user.edit', compact('user'));
    }

    /**
     * @param UpdateUser $request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUser $request, User $user)
    {
        DB::transaction(function () use ($request, $user)
        {
            $data = $request->data();

            $user->update($data);
        });

        return back()->with('Successfully updated');
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function destroy(User $user)
    {
        $user->delete();

        return back()->withSuccess('User has been successfully deleted.');
    }
}
