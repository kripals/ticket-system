<?php
/**
 * Created by PhpStorm.
 * User: mell
 * Date: 3/23/2018
 * Time: 4:19 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:3|unique:users,username',
            'email'    => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed'
        ];
    }

    /**
     * @return array
     */
    public function data()
    {
        $data = [
            'email'      => trim($this->get('email')),
            'username'   => str_slug($this->get('username')),
            'password'   => bcrypt($this->get('password')),
            'first_name' => $this->has('first_name') ? trim($this->get('first_name')) : null,
            'last_name'  => $this->has('last_name') ? trim($this->get('last_name')) : null,
            'address'    => $this->has('address') ? trim($this->get('address')) : null,
            'contact_no' => $this->has('contact_no') ? trim($this->get('contact_no')) : null
        ];

        return $data;
    }
}
