<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * @return array
     */
    public function data()
    {
        if ($this->has('first_name'))
        {
            $data['first_name'] = trim($this->get('first_name'));
        }

        if ($this->has('last_name'))
        {
            $data['last_name'] = trim($this->get('last_name'));
        }

        if ($this->has('address'))
        {
            $data['address'] = trim($this->get('address'));
        }

        if ($this->has('contact_no'))
        {
            $data['contact_no'] = trim($this->get('contact_no'));
        }

        return $data;
    }
}
